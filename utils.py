import os
from collections import defaultdict

import toolforge
import unicodedata
from SPARQLWrapper import SPARQLWrapper, JSON

import tfsl
import queries

QUERIES = "queries"

user_agent = toolforge.set_user_agent(
    'hangor',
    email='mahir256@live.com')

sparql = SPARQLWrapper("https://query.wikidata.org/sparql", agent=user_agent)

def retrieve_label(qid, interface_language):
    current_item = tfsl.Q_(qid)
    try:
        return current_item.get_label(tfsl.langs.find(interface_language)[0]).text
    except Exception as e:
        return qid

def construct_labeled_qid(qid, interface_language):
    return {
        "qid": qid,
        "label": retrieve_label(qid, interface_language)
    }

def construct_labeled_lsid(lsid, interface_language):
    target_lexeme = tfsl.L_(lsid)
    return {
        "lsid": lsid,
        "lid": target_lexeme.lexeme_id,
        "lemmata": target_lexeme.lemmata.__jsonout__(),
        "gloss": retrieve_gloss(target_lexeme[lsid], interface_language)
    }

def construct_forms(lexeme, interface_language):
    forms = []

    for form in lexeme.get_forms():
        new_form = {
            "id": form.id,
            "value": form.representations.__jsonout__(),
            "features": sorted([retrieve_label(feat, interface_language) for feat in form.features])
        }
        new_form["audio"] = [
           claim.get_str().replace(' ','_') for claim in form["P443"]
        ]
        forms.append(new_form)

    return forms

ext_id_abbreviations = [
    ('P5275', 'oed', "http://www.oed.com/view/Entry/{}"),
    ('P7572', 'dcs', "http://www.sanskrit-linguistics.org/dcs/index.php?contents=lemma&IDWord={}"),
    ('P10041', 'ordbøkene', "https://ordbøkene.no/nn/{}"),
    ('P10042', 'ordbøkene', "https://ordbøkene.no/bm/{}"),
    ('P11055', 'diacl', "https://diacl.ht.lu.se/Lexeme/Details/{}"),
    ('P11068', 'favereau', 'http://www.arkaevraz.net/dicobzh/index.php?b_lang=1&b_kw={}'),
    ('P11124', 'krdict', "https://krdict.korean.go.kr/dicSearch/SearchView?divSearch=defView&ParaWordNo={}"),
    ('P11125', 'stdict', "https://stdict.korean.go.kr/search/searchView.do?searchKeywordTo=3&word_no={}"),
    ('P11261', 'toli.query', "https://toli.query.mn/dictionary_items/{}"),
    ('P11262', 'toli.gov', "http://toli.gov.mn/w/{}"),
    ('P11264', 'mongoltoli', "https://mongoltoli.mn/dictionary/detail/{}"),
    ('P11275', 'manchu.work', "http://manchu.work/dicts/{}"),
    ('P11328', 'dehkhoda', "https://dehkhoda.ut.ac.ir/fa/dictionary/detail/{}"),
    ('P11350', 'udb', "http://udb.gov.pk/result_details.php?word={}"),
    ('P11519', 'elexiko', "https://www.owid.de/artikel/{}"),
]

def get_external_ids(lexeme):
    external_ids = defaultdict(list)
    for property_id, abbreviation, url_scheme in ext_id_abbreviations:
        for statement in lexeme[property_id]:
            external_ids[property_id].append({
                "name": abbreviation,
                "value": url_scheme.format(statement.value)
            })
    return dict(external_ids)

def construct_combines(current_lexeme):
    parts = []
    for statement in current_lexeme['P5238']:
        target_lid = statement.get_ItemValue().get_Lid()
        target_lexeme = tfsl.L_(target_lid)
        part = {
            'language': target_lexeme.language,
            'lid': target_lid,
            'lemmata': target_lexeme.lemmata.__jsonout__()
        }
        parts.append(part)
    return parts

def construct_derived_from(current_lexeme):
    parts = []
    for statement in current_lexeme['P5191']:
        target_lid = statement.get_ItemValue().get_Lid()
        target_lexeme = tfsl.L_(target_lid)
        part = {
            'language': target_lexeme.language,
            'lid': target_lid,
            'lemmata': target_lexeme.lemmata.__jsonout__()
        }
        parts.append(part)
    return parts

possible_forms_templates = ['forms_Q9027_Q1084.html']

def categorize_forms(current_lexeme, interface_language):
    form_dict = defaultdict(list)
    for form in current_lexeme.get_forms():
        sort_key = "_".join(sorted(feat for feat in form.features))
        new_form = {
            "id": form.id,
            "value": form.representations.__jsonout__(),
            "features": sorted([retrieve_label(feat, interface_language) for feat in form.features])
        }
        new_form["audio"] = [
           claim.get_str().replace(' ','_') for claim in form["P443"]
        ]
        form_dict[sort_key].append(new_form)
    return form_dict


def construct_word(lid, interface_language):
    current_lexeme = tfsl.L_(lid)
    candidate_forms_template = "forms_{}_{}.html".format(current_lexeme.language.item, current_lexeme.category)
    if candidate_forms_template in possible_forms_templates:
        constructed_forms = categorize_forms(current_lexeme, interface_language)
    else:
        candidate_forms_template = ""
        constructed_forms = construct_forms(current_lexeme, interface_language)
    word = {
        "id": lid,
        "ext_ids": get_external_ids(current_lexeme),
        "lemmata": current_lexeme.lemmata.__jsonout__(),
        "language": construct_labeled_qid(current_lexeme.language.item, interface_language),
        "pos": construct_labeled_qid(current_lexeme.category, interface_language),
        "gender": [construct_labeled_qid(
            claim.get_ItemValue().get_Qid(),
            interface_language) for claim in current_lexeme['P5185']],
        "combines": construct_combines(current_lexeme),
        "senses": construct_senses(current_lexeme, interface_language),
        "examples": [],
        "compounds": [],
        "derived_from": construct_derived_from(current_lexeme),
        "forms": constructed_forms,
        "forms_template": candidate_forms_template
    }

    return word

def construct_example(raw_example):
    example = {"year": "", "value": "", "title": "", "source_id": ""}
    if raw_example.get("note"):
        example["year"] = raw_example["note"]["value"]
    if raw_example.get("sourceLabel"):
        example["title"] = raw_example["sourceLabel"]["value"]
    if raw_example.get("source"):
        example["source_id"] = raw_example["source"]["value"]
    if raw_example.get("sourceLabel"):
        example["title"] = raw_example["sourceLabel"]["value"]
    example["value"] = raw_example["value_"]["value"]
    return example

def construct_relations_items(sense, interface_language):
    relations = {}
    for property_id in ['P5137', 'P9970']: # properties with Wikidata items as values
        if not sense.haswbstatement(property_id):
            continue
        property_name = construct_labeled_qid(property_id, interface_language)
        relations[property_id] = {'name': property_name, 'relations': []}
        for statement in sense[property_id]:
            statement_qid = statement.get_ItemValue().get_Qid()
            relations[property_id]['relations'].append(construct_labeled_qid(statement_qid, interface_language))
    return relations

def construct_relations_senses(sense, interface_language):
    relations = {}
    for property_id in ['P5973', 'P5972']: # properties with Wikidata senses as values
        if not sense.haswbstatement(property_id):
            continue
        property_name = construct_labeled_qid(property_id, interface_language)
        relations[property_id] = {'name': property_name, 'relations': []}
        for statement in sense[property_id]:
            statement_lsid = statement.get_ItemValue().get_LSid()
            relations[property_id]['relations'].append(construct_labeled_lsid(statement_lsid, interface_language))
    return relations

def retrieve_gloss(sense, interface_language):
    try:
        retrieved_gloss = next(gloss.__jsonout__() for gloss in sense.glosses if gloss.language.code == interface_language)
    except StopIteration:
        retrieved_gloss = next(gloss.__jsonout__() for gloss in sense.glosses)
    return retrieved_gloss

def construct_senses(lexeme, interface_language):
    senses = []

    for sense in lexeme.get_senses():
        sense_out = {
            "id": sense.id,
            "gloss": retrieve_gloss(sense, interface_language),
            "relations_items": construct_relations_items(sense, interface_language),
            "relations_senses": construct_relations_senses(sense, interface_language),
            "examples": []
        }
        senses.append(sense_out)
    return senses

def run_sparql(query):
    sparql.setQuery(query)
    sparql.setReturnFormat(JSON)
    results = sparql.query().convert()
    return results["results"]["bindings"]

def get_query(query_name):
    with open(os.path.join(QUERIES, '{}.rq'.format(query_name))) as query_file:
        return query_file.read()

def get_lexemes_from_form(lang_item, word):
    word = unicodedata.normalize('NFC',word)
    if tfsl.interfaces.is_Qid(lang_item):
        query_result = run_sparql(get_query("get_lexeme_id_from_form_item") % (lang_item, word))
    else:
        query_result = run_sparql(get_query("get_lexeme_id_from_form_code") % (word, lang_item))
    return [x["l"]["value"].replace('http://www.wikidata.org/entity/','') for x in query_result]

def get_words_in_language(lang, pos, sense, offset=0, limit=100):

    lang_is_qid = lang is not None and tfsl.interfaces.is_Qid(lang)
    pos_is_provided = pos is not None

    query_choices = {
        (True, False, "false"): "get_words_in_language_without_senses_item",
        (True, False, "true"): "get_words_in_language_with_senses_item",
        (True, False, "all"): "get_words_in_language_item",
        (False, False, "false"): "get_words_in_language_without_senses_code",
        (False, False, "true"): "get_words_in_language_with_senses_code",
        (False, False, "all"): "get_words_in_language_code",
        (False, True, "false"): queries.lemmata_with_pos_false_code,
        (False, True, "true"): queries.lemmata_with_pos_true_code,
        (False, True, "all"): queries.lemmata_with_pos_all_code,
        (True, True, "false"): queries.lemmata_with_pos_false_item,
        (True, True, "true"): queries.lemmata_with_pos_true_item,
        (True, True, "all"): queries.lemmata_with_pos_all_item,
    }

    if pos_is_provided:
        query = query_choices[lang_is_qid, pos_is_provided, sense] % (lang, pos, offset, limit)
    else:
        query = get_query(query_choices[lang_is_qid, pos_is_provided, sense]) % (lang, offset, limit)

    results = run_sparql(query)
    return [x["lemma"]["value"] for x in results]

