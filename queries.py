
lemmata_with_pos_all_code = """
SELECT DISTINCT ?lemma
WITH {
  SELECT ?language {
    ?language wdt:P218|wdt:P219|wdt:P305 "%s" .
  }
} AS %%languages
WHERE {
    INCLUDE %%languages .
    ?l dct:language ?language ;
       wikibase:lemma ?lemma ; 
       wikibase:lexicalCategory wd:%s ..
}
ORDER BY LCASE(str(?lemma))
OFFSET %d
LIMIT %d
"""
lemmata_with_pos_true_code = """
SELECT DISTINCT ?lemma
WITH {
  SELECT ?language {
    ?language wdt:P218|wdt:P219|wdt:P305 "%s" .
  }
} AS %%languages
WHERE {
    INCLUDE %%languages .
    ?l dct:language ?language ;
       wikibase:lemma ?lemma ;
       wikibase:lexicalCategory wd:%s ;
       ontolex:sense [] .
}
ORDER BY LCASE(str(?lemma))
OFFSET %d
LIMIT %d
"""
lemmata_with_pos_false_code = """
SELECT DISTINCT ?lemma
WITH {
  SELECT ?language {
    ?language wdt:P218|wdt:P219|wdt:P305 "%s" .
  }
} AS %%languages
WHERE {
    INCLUDE %%languages .
    ?l dct:language ?language ;
       wikibase:lemma ?lemma ;
       wikibase:lexicalCategory wd:%s .
    MINUS { ?l ontolex:sense [] }
}
ORDER BY LCASE(str(?lemma))
OFFSET %d
LIMIT %d
"""
lemmata_with_pos_all_item = """
SELECT DISTINCT ?lemma WHERE {
    ?l dct:language wd:%s ;
       wikibase:lemma ?lemma ; 
       wikibase:lexicalCategory wd:%s .
}
ORDER BY LCASE(str(?lemma))
OFFSET %d
LIMIT %d
"""
lemmata_with_pos_true_item = """
SELECT DISTINCT ?lemma WHERE {
    ?l dct:language wd:%s ;
       wikibase:lemma ?lemma ;
       wikibase:lexicalCategory wd:%s ;
       ontolex:sense [] .
}
ORDER BY LCASE(str(?lemma))
OFFSET %d
LIMIT %d
"""
lemmata_with_pos_false_item = """
SELECT DISTINCT ?lemma WHERE {
    ?l dct:language wd:%s ;
       wikibase:lemma ?lemma ;
       wikibase:lexicalCategory wd:%s .
    MINUS { ?l ontolex:sense [] }
}
ORDER BY LCASE(str(?lemma))
OFFSET %d
LIMIT %d
"""
